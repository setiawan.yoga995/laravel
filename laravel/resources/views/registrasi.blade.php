<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <h1>Buat Account Baru</h1>
  <h2>Sign Up Form</h2>
  <form action="/welcome" method="post">
  @csrf  
  <label>
      First Name : <br><br>
      <input type="text" name="firstname">
    </label>
    <br><br>
    <label>
      Last Name : <br><br>
      <input type="text" name="lastname">
    </label>
    <br><br>
    <label>
      Gender <br><br>
      <input type="radio" name="kelamin">Male<br>
      <input type="radio" name="kelamin">Female<br>
    </label>
    <br><br>
    <label>
      Nationality <br><br>
      <select name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="amerika">Amerika</option>
        <option value="inggris">Inggris</option>
      </select>
    </label>
    <br><br>
    <label>
      Languange Spoken <br><br>
      <input type="checkbox">Bahasa Indonesia<br>
      <input type="checkbox">English<br>
      <input type="checkbox">Other
    </label>
    <br><br>
    <label>
      Bio <br><br>
      <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
    </label>
    <br>
    <input type="submit" name="signup" value="Sign Up">
  </form>
</body>

</html>