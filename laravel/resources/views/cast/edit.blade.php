@extends('layout.master')
@section('judul')
<h1>Halaman Edit</h1>
@endsection
@section('isi')
<div>
  <h2>Edit cast {{$cast->id}}</h2>
  <form action="/cast/{{$cast->id}}" method="post">
      @csrf
      @method('PUT')
      <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="nama" placeholder="Masukkan nama anda">
          @error('nama')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
      </div>
      <div class="form-group">
          <label for="umur">Umur</label>
          <input type="text" class="form-control" name="umur"  value="{{$cast->umur}}"  id="umur" placeholder="Masukkan umur anda">
          @error('umur')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
      </div>
      <div class="form-group">
        <label for="bio">Bio</label>
        <textarea name="bio" id="bio" class="form-control" placeholder="Masukkan Bio Anda"> {{$cast->bio}}</textarea>
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
      <button type="submit" class="btn btn-primary">Edit</button>
  </form>
</div>
@endsection