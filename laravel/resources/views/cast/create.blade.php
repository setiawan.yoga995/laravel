@extends('layout.master')
@section('judul')
<h1>Halaman Tambah Cast</h1>
@endsection
@section('isi')
<div>
  <h2>Tambah Data</h2>
      <form action="/cast" method="post">
          @csrf
          <div class="form-group">
              <label for="nama">Nama</label>
              <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Anda">
              @error('nama')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
          </div>
          <div class="form-group">
              <label for="umur">Umur</label>
              <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur Anda">
              @error('umur')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
          </div>
          <div class="form-group">
            <label for="bio">Bio</label><br>
            <textarea name="bio" id="bio" class="form-control" placeholder="Masukkan Bio Anda"> </textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
          <button type="submit" class="btn btn-primary">Tambah</button>
      </form>
</div>

@endsection